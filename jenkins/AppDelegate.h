//
//  AppDelegate.h
//  jenkins
//
//  Created by Thomas Carayol on 05/04/16.
//  Copyright © 2016 Appstud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

