//
//  main.m
//  jenkins
//
//  Created by Thomas Carayol on 05/04/16.
//  Copyright © 2016 Appstud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
