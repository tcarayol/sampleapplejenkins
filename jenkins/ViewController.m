//
//  ViewController.m
//  jenkins
//
//  Created by Thomas Carayol on 05/04/16.
//  Copyright © 2016 Appstud. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)plusOne:(NSInteger)var {
    return var + 1;
}

@end
