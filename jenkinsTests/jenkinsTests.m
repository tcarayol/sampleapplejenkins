//
//  jenkinsTests.m
//  jenkinsTests
//
//  Created by Thomas Carayol on 05/04/16.
//  Copyright © 2016 Appstud. All rights reserved.
//

#import <Kiwi.h>

SPEC_BEGIN(MathSpec)

describe(@"MathSuccess", ^{
    it(@"success", ^{
        NSUInteger a = 16;
        NSUInteger b = 26;
        [[theValue(a + b) should] equal:theValue(42)];
    });
});

SPEC_END